import 'package:flutter/material.dart';

import 'main.dart';

void main(){
  runApp(new MyApp());
}
class Reminderpage extends StatefulWidget
{

  State<StatefulWidget> createState(){
    return _Reminderpage();
  }

}
class _Reminderpage extends State<Reminderpage> {

//design
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("REMINDER"),

      ),
      body: new Container(

        margin: EdgeInsets.all(0.0),
        child: new Form(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: Buttons(),
            )
        ),

      ),
    );

    return null;
  }
}

  List <Widget> Buttons (){

    return[
      SizedBox(height: 20.0,),
      new FlatButton
        (
        child:new Text("ADD NEW REMINDER             + ",style:new TextStyle(fontSize: 20.0),),
        textColor: Colors.red,
        onPressed: MovetoForgotPassword,

      ),

      SizedBox(height: 20.0,),
      Searchbar(),
      SizedBox(height: 40.0,),
    ];
  }

Widget Searchbar(){
  BoxDecoration(
    shape: BoxShape.circle,
    color: Color.fromARGB(50,255, 255, 255),
    borderRadius: BorderRadius.all(Radius.circular(22.0)),
  );
  Expanded(child: Row(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Expanded(
        flex: 1,
        child: TextFormField(
          decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Search Reminders",
            hintStyle: TextStyle(color: Colors.black),
            icon: Icon(Icons.search, color: Colors.black,),
          ),
        ),
      )
    ],
  )

  );
}
//Widget text_()=> Text("______________________ OR _____________________",textAlign: TextAlign.center,
//);



void MovetoForgotPassword() {

}

void validateandsave() {
}
void autheticateandlog(){

}

