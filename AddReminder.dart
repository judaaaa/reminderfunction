import 'package:flutter/material.dart';
import 'dart:async';
import 'main.dart';
//import 'package:cloud_firestore/cloud_firestore.dart';

void main(){
  runApp(new MyApp());
}
class addReminderpage extends StatefulWidget
{

  State<StatefulWidget> createState(){
    return _addReminderpage();
  }

}
class _addReminderpage extends State<addReminderpage> {

  String title, description;

  getTitel(title) {
    this.title = title;
  }

  getDescription(description) {
    this.description = description;
  }

  AddDBReminderDetails(){
  //  DocumentReference documentReference = Firestore.instance.collection('AddReminder').document(title);
  Map<String,dynamic> data ={
    "title":title,
    "description":description,
  };
  //documentReference.setData(data).whenComplete((){
    print("reminder entered");
  //});
  }
  //final DocumentReference documentReference = Firestore.instance.collection("AddR").document("re");
  DateTime _date = new DateTime.now();
  TimeOfDay _time = new TimeOfDay.now();

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2019),
        lastDate: new DateTime(3000)
    );
    if (picked != null && picked != _date) {
      print('${_date.toString()}');
      setState(() {
        _date = picked;
      });
    }
  }

  Future<Null> selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: _time);

    if (picked != null && picked != _time) {
      print('${_time.toString()}');
      setState(() {
        _time = picked;
      });
    }
  }

//design
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(
          "                     REMINDERS", textAlign: TextAlign.center,),
      ),

      body: new Container(
        //color: Colors.redAccent,
        //t child: new Text(':${_date.toString()}'),
        margin: EdgeInsets.all(10.0),

        child: new Form(
            child: new Column(

              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: Inputs() + Buttons(),

            )
        ),
      ),
    );
    return null;
  }


  List <Widget> Inputs() {
    return [
      SizedBox(height: 10.0,),
      Logo(),
      SizedBox(height: 30.0,),
      new TextField(
        onChanged: (String title) {
          getTitel(title);
        },
        decoration: new InputDecoration(labelText: 'Title',
            labelStyle: TextStyle(fontWeight: FontWeight.bold)),

      ),
      SizedBox(height: 20.0,),
      new TextField(
        onChanged: (String description) {
          getDescription(description);
        },
        decoration: new InputDecoration(labelText: 'Reminder Text',
            labelStyle: TextStyle(fontWeight: FontWeight.bold)),
      ),
      SizedBox(height: 20.0,),
    ];
  }

  Widget Logo() {
    return new Hero(
      tag: 'hero',

      child: new CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 50.0,
        child: Image.asset('images/logo.png'),
      ),
    );
  }

  List <Widget> Buttons() {
    return [
      //new Text('${_date.toString()}'),
      new FlatButton(
          child: new Text('Select Date',
            style: new TextStyle(fontWeight: FontWeight.bold),
            textAlign: TextAlign.left,),
          textColor: Colors.white,
          color: Colors.redAccent,
          onPressed: () {
            selectDate(context);
          }),
      new TextField(
        decoration: new InputDecoration(hintText: '${_date.toString()}',hintStyle: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.black,
        )
        ),
      ),
      new FlatButton(
          child: new Text('Select Time',
            style: new TextStyle(fontWeight: FontWeight.bold),
            textAlign: TextAlign.justify,),
          textColor: Colors.white,
          color: Colors.redAccent,
          onPressed: () {
            selectTime(context);
          }),
      new TextField(
        decoration: new InputDecoration(hintText: '${_time.toString()}',hintStyle: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.black,
        )
        ),
      ),
      SizedBox(height: 40.0,),
      new FlatButton
        (
        child: new Text("ADD", style: new TextStyle(fontSize: 12.0)),
        textColor: Colors.red,
        onPressed: (){
          AddDBReminderDetails();
          },
      ),

      SizedBox(height: 20.0,),

      SizedBox(height: 40.0,),
    ];
  }


}






